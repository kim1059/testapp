package test.testapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.testapp.R;
import test.testapp.adapters.QuestionsAdapter;
import test.testapp.models.Question;
import test.testapp.models.SearchResponse;
import test.testapp.retrofit.ApiClient;
import test.testapp.retrofit.ApiInterface;
import test.testapp.utils.Constants;

public class MainActivity extends AppCompatActivity
        implements QuestionsAdapter.ItemClickListener {

    @BindView(R.id.edt_text)
    EditText edtText;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    private QuestionsAdapter questionsAdapter;
    private List<Question> questionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        questionsAdapter = new QuestionsAdapter(questionList);
        questionsAdapter.setClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvList.getContext(),
                layoutManager.getOrientation());

        rvList.addItemDecoration(dividerItemDecoration);
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(layoutManager);
        rvList.setAdapter(questionsAdapter);
    }

    @Override
    public void onClick(View view, int position) {
        Intent intent = new Intent(MainActivity.this, AnswersActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_QUESTION, questionList.get(position));
        startActivity(intent);
    }

    @OnClick(R.id.btn_submit)
    public void onSubmit() {
        String text = edtText.getText().toString();
        if (text.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter text",
                    Toast.LENGTH_LONG).show();
            return;
        }

        loadData(text);
    }

    private void loadData(String text) {
        showProgress();
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<SearchResponse> call = apiService.getQuestions(text);
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse>call, Response<SearchResponse> response) {
                hideProgress();
                questionList = response.body().getData().getQuestions();
                if (questionList == null) {
                    Toast.makeText(getApplicationContext(), "No questions",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                questionsAdapter.setData(questionList);
            }

            @Override
            public void onFailure(Call<SearchResponse>call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void showProgress() {
        rvList.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        rvList.setVisibility(View.VISIBLE);
        pbProgress.setVisibility(View.GONE);
    }
}

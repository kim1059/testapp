package test.testapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kim1059 on 15.03.2017.
 */

public class SearchResponse {
    @SerializedName("data")
    private SearchResponseData data;


    public SearchResponseData getData() {
        return data;
    }

    public void setData(SearchResponseData data) {
        this.data = data;
    }
}

package test.testapp.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import test.testapp.models.SearchResponse;

/**
 * Created by kim1059 on 15.03.2017.
 */

public interface ApiInterface {
    @GET("search/question/lecturio_com")
    Call<SearchResponse> getQuestions(@Query("q") String text);

}

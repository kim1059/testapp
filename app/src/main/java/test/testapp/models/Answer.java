package test.testapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kim1059 on 15.03.2017.
 */

public class Answer implements Serializable{
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

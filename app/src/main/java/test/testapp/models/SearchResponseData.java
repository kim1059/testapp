package test.testapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by kim1059 on 15.03.2017.
 */

public class SearchResponseData {
    @SerializedName("questions")
    private ArrayList<Question> questions;


    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
}

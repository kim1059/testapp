package test.testapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.testapp.R;
import test.testapp.adapters.AnswersAdapter;
import test.testapp.models.Answer;
import test.testapp.models.Question;
import test.testapp.utils.Constants;

public class AnswersActivity extends AppCompatActivity {

    @BindView(R.id.rv_list)
    RecyclerView rvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answers);
        ButterKnife.bind(this);

        Question question = (Question) getIntent().getSerializableExtra(Constants.INTENT_EXTRA_QUESTION);
        List<Answer> answers = question.getAnswers();
        if (answers == null) {
            Toast.makeText(getApplicationContext(), "No answers",
                    Toast.LENGTH_LONG).show();
            return;
        }

        AnswersAdapter answersAdapter = new AnswersAdapter(answers);

        LinearLayoutManager layoutManager = new LinearLayoutManager(AnswersActivity.this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvList.getContext(),
                layoutManager.getOrientation());

        rvList.addItemDecoration(dividerItemDecoration);
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(layoutManager);
        rvList.setAdapter(answersAdapter);
    }
}

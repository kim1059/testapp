package test.testapp.utils;

/**
 * Created by kim1059 on 15.03.2017.
 */

public class Constants {
    /*Base Url*/
    public static final String BASE_URL = "https://www.lecturio.de/api/en/v7/android/";

    public static String INTENT_EXTRA_QUESTION = "extra_question";
}
